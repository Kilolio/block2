package work2.partTwo;

import java.util.Objects;

public class Payment {
    String name;
    private int payDay = 0;
    private int payMonth = 0;
    private int payYear = 0;
    private int ruble = 0;

    public Payment(String fullName, int day, int month, int year, int pay) {
        this.name = fullName;
        this.payDay = day;
        this.payMonth = month;
        this.payYear = year;
        this.ruble = pay;
    }

    public Payment() {
        this.name = "fullName";
        this.payDay = 1;
        this.payMonth = 1;
        this.payYear = 1918;
        this.ruble = 0;
    }

    public String getName() {
        return name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return payDay == payment.payDay &&
                payMonth == payment.payMonth &&
                payYear == payment.payYear &&
                ruble == payment.ruble &&
                Objects.equals(name, payment.name);
    }

    @Override
    public String toString() {
        return "Payment{" +
                "name='" + name + '\'' +
                ", payDay=" + payDay +
                ", payMonth=" + payMonth +
                ", payYear=" + payYear +
                ", ruble=" + ruble +
                ", }\n";
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, payDay, payMonth, payYear, ruble);
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPayDay() {
        return payDay;
    }

    public void setPayDay(int payDay) {
        this.payDay = payDay;
    }

    public int getPayMonth() {
        return payMonth;
    }

    public void setPayMonth(int payMonth) {
        this.payMonth = payMonth;
    }

    public int getPayYear() {
        return payYear;
    }

    public void setPayYear(int payYear) {
        this.payYear = payYear;
    }

    public int getRuble() {
        return ruble;
    }

    public void setRuble(int ruble) {
        this.ruble = ruble;
    }


}
