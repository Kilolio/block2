package work2.partTwo;

import org.jetbrains.annotations.NotNull;

public class FinanceReportProcessor {

    @NotNull
    public static FinanceReport getPayFamily(FinanceReport financeReport, char c) {
        int k = 0;
        FinanceReport report = new FinanceReport(financeReport.getSize()-1);
        for (int i = 0; i < financeReport.getSize()-1; i++) {
            char s = financeReport.getIPay(i).getName().charAt(0);
            if (s == c) {
                report.setIPay(financeReport.getPayArray()[i], k);
                k++;
            }
        }
        return report;
    }

    @NotNull
    public static FinanceReport getPayCost(FinanceReport financeReport, int n){
        int k = 0;
        FinanceReport report = new FinanceReport(financeReport.getSize()-2);
        for(int i =0; i<financeReport.getSize(); i++){
            if(financeReport.getIPay(i).getPayDay()>n){
                report.setIPay(financeReport.getPayArray()[i],k);
                k++;
            }
        }
        return report;
    }
}
