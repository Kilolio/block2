package work2.partTwo;

public class FinanceReport {
    private Payment[] pay;
    private String byNameReport;
    private int day, month, year;

    public FinanceReport(Payment[] pay, String byNameReport, int day, int month, int year) {
        this.pay = pay;
        this.byNameReport = byNameReport;
        this.day = day;
        this.month = month;
        this.year = year;
    }


    public void setByNameReport(String byNameReport) {
        this.byNameReport = byNameReport;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public FinanceReport() {
        return;
    }

    public String getByNameReport() {
        return byNameReport;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public FinanceReport(int n) {
        this.pay = new Payment[n];
    }

    public FinanceReport(FinanceReport finRep) {
        pay = new Payment[finRep.getSize()];
        for (int i = 0; i < pay.length; i++) {
            pay[i] = finRep.getIPay(i);
        }
        this.byNameReport = finRep.getByNameReport();
        this.day = finRep.getDay();
        this.month = finRep.getMonth();
        this.year = finRep.getYear();
    }

    public Payment[] getPayArray() {
        return pay;
    }

    public void setPayArray(Payment[] pay) {
        this.pay = pay;
        this.day = 1;
        this.month = 1;
        this.year = 1;
        this.byNameReport = "Составитель";
    }

    /**
     *
     * @return получение количества платежей
     */
    public int getSize() {
        return pay.length;
    }


    public Payment getIPay(int i) {
        return pay[i];
    }

    public void setIPay(Payment p, int i) {
        this.pay[i] = p;
    }

    public String getFullNameN(Payment[] payments, int i) {
        return payments[i].getName();
    }

    @Override
    public String toString() {
        String s = String.format("[Автор: %s, ", this.byNameReport) + String.format(" дата %02d.%02d.%04d, ", this.day, this.month, this.year);
        StringBuilder stringBuilder = new StringBuilder();
        {
            for (Payment payment : pay) {
                if (payment != null) {
                    stringBuilder = stringBuilder.append(payment.toString());
                }
            }
            return s + " Платежи[" + "\n" + stringBuilder + "]]";
        }
    }
}