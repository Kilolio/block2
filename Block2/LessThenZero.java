package work2;
/**
 * if number of string < 0 - exception
 */
public class LessThenZero extends Exception {

        /**
         * @param message -message
         */
        public LessThenZero(final String message) {
            super(message);
        }

        /**
         * @param message - message
         * @param e       - exception
         */
        public LessThenZero(final String message, final Exception e) {
            super(message, e);

    }
}
