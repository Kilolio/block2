package work2;

class StringProcessor {
    private StringBuilder sb;

    StringProcessor() {
        sb = new StringBuilder();
    }

    String copyString(String s, int n) throws LessThenZero {
        if (n < 0) {
            throw new LessThenZero("number of string < 0");
        }
        sb.append("String copy n times : ").append( new String(new char[n]).replace("\0", s));

        return sb.toString();
    }

    int howMuchStingFind(String a, String b) {
        int n = 0;
        int i = -1;
        while (i < a.length() - 1) {
            for (int j = 0; j < b.length(); j++) {
                i++;
                if (i < a.length()) {
                    if (a.charAt(i) != b.charAt(j)) break;
                    if (j == b.length() - 1) n++;
                }
            }
        }
        return n;
    }

    String changeIntToString(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch (s.charAt(i)) {
                case '1':
                    sb.append("один ");
                    break;
                case '2':
                    sb.append("два ");
                    break;
                case '3':
                    sb.append("три ");
                    break;
                default:
                    sb.append("null ");
            }
        }
        return sb.toString();
    }

    String deleteEachTwo(String s) {
        StringBuilder sb = new StringBuilder();
        sb.append(s);
        for (int i = 0; i < sb.length(); i++) {
            sb.delete(i + 1, i + 2);
        }
        return sb.toString();
    }

    String hexToInt(String s) {
        boolean n = false;
        StringBuilder stringBuilder = new StringBuilder();
        String b = "0x";
        int i = -1;
        while (i < s.length() - 1) {
            if (!n) {
                for (int j = 0; j < b.length(); j++) {
                    i++;
                    if (i < s.length()) {
                        if (s.charAt(i) != b.charAt(j)) {
                            stringBuilder.append(s.charAt(i));
                            break;
                        }
                        if (j == b.length() - 1) n = true;
                    }
                }
            } else {
                i++;
                if (s.charAt(i) != '0') {
                    stringBuilder.append(s.charAt(i));
                }
                else if(!s.matches("[-+]?\\d+")){
                    stringBuilder.append(s.charAt(i));
                }
            }
        }
        return stringBuilder.toString();
    }
}
